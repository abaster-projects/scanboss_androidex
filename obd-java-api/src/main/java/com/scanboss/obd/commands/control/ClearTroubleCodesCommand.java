package com.scanboss.obd.commands.control;

import com.scanboss.obd.commands.ObdCommand;
import com.scanboss.obd.enums.AvailableCommandNames;

public class ClearTroubleCodesCommand extends ObdCommand {
    String dtcsStatus = "NotCleared";

    public ClearTroubleCodesCommand() {
        super("04");
    }

    public ClearTroubleCodesCommand(ClearTroubleCodesCommand other) {
        super(other);
    }

    @Override
    protected void performCalculations() {
        final String result = getResult();

        if (result.equals("44")) {
            dtcsStatus = "Cleared";
        }
    }

    @Override
    public String getFormattedResult() {
        return String.valueOf(dtcsStatus);
    }

    @Override
    public String getCalculatedResult() {
        return String.valueOf(dtcsStatus);
    }

    @Override
    public String getName() {
        return AvailableCommandNames.CLEAR_TROUBLE_CODES.getValue();
    }
}
