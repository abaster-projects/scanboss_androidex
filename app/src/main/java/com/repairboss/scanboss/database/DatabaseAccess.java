package com.repairboss.scanboss.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public tblDTC getDTC(String code) {

        Cursor cursor = database.query(tblDTC.TABLE_NAME,
                new String[]{tblDTC.COLUMN_ID, tblDTC.COLUMN_CODE, tblDTC.COLUMN_DESCRIPTION},
                tblDTC.COLUMN_CODE + "=?",
                new String[]{code}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        tblDTC dtc = null;

        if (cursor.getCount() > 0) {
            // prepare note object
            dtc = new tblDTC(
                    cursor.getInt(cursor.getColumnIndex(tblDTC.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(tblDTC.COLUMN_CODE)),
                    cursor.getString(cursor.getColumnIndex(tblDTC.COLUMN_DESCRIPTION)));
        }

        // close the db connection
        cursor.close();

        return dtc;
    }
}
