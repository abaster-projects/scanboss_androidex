package com.repairboss.scanboss.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.repairboss.scanboss.database.DatabaseAccess;
import com.repairboss.scanboss.database.DatabaseHelper;
import com.repairboss.scanboss.database.tblDTC;
import com.repairboss.scanboss.io.AbstractGatewayService;
import com.repairboss.scanboss.io.DTCsAdapter;
import com.repairboss.scanboss.io.ObdCommandJob;
import com.repairboss.scanboss.io.ObdGatewayService;
import com.scanboss.obd.commands.control.ClearTroubleCodesCommand;
import com.scanboss.obd.commands.control.TroubleCodesCommand;
import com.scanboss.obd.commands.control.VinCommand;
import com.scanboss.obd.enums.AvailableCommandNames;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DTCsActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 3;

    private BluetoothAdapter mBluetoothAdapter = null;

    //private APIService apiService;
    private AbstractGatewayService service;
    private boolean isServiceBound;

    private DatabaseHelper db;
    DatabaseAccess databaseAccess;

    //List<DTC> dtcs = new ArrayList<DTC>();
    //DTCs dtcs = new DTCs();
    Map<String, String> dtcs = new HashMap<String, String>();

    RecyclerView recyclerView;

    int keepRunning;

    int count;

    String VIN;

    Button btnDTC;

    List<Integer> systemIDs = new ArrayList<Integer>();
    List<String> systems = new ArrayList<String>();

    List<Integer> groupIDs = new ArrayList<Integer>();
    List<String> groups = new ArrayList<String>();

    List<Integer> subGroupIDs = new ArrayList<Integer>();
    List<String> subGroups = new ArrayList<String>();

    //List<Taxonomy> taxonomy = new ArrayList<Taxonomy>();

    //List<Repair> repairs = new ArrayList<Repair>();

    Integer baseVehicleID = 22124;
    Integer engineID = 2913;
    Integer subModelID = 1549;
    Integer systemID[] = {7, 7, 7, 7};
    Integer groupID[] = {20, 20, 20, 20};
    Integer subGroupID[] = {35, 56, 15, 108};

    Integer maxL = 0;

    @Inject
    SharedPreferences prefs;

    private final Runnable mmmQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                service.queueJob(new ObdCommandJob(new VinCommand()));
                //service.queueJob(new ObdCommandJob(new TroubleCodesCommand()));
            }
            // run again in period defined in preferences
            if (keepRunning == 1) {
                new Handler().postDelayed(mmmQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
            }
            //new Handler().postDelayed(mQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.MaterialTheme);
        setContentView(R.layout.activity_dtcs);
        prefs = getSharedPreferences("com.repairboss.scanboss_preferences", MODE_PRIVATE);

        keepRunning = 1;

        count = 0;

        databaseAccess = DatabaseAccess.getInstance(this);

        btnDTC = findViewById(R.id.btnSendDTC);

        recyclerView = findViewById(R.id.dtcsRecyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initializeTaxonomies();

        btnDTC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendDTCs();
                //getRepairs();
                clearDTCs();
            }
        });

        btnDTC.setEnabled(false);

        //apiService = APIClient.getClient().create(APIService.class);

        final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
        if (connectionType.equals("BLUETOOTH")) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
                finish();
            }

            if (mBluetoothAdapter == null) {
                return;
            }
            // If BT is not on, request that it be enabled.
            // setupChat() will then be called during onActivityResult
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } else {

                doBindService();

                new Handler().post(mmmQueueCommands);
            }
        } else if (connectionType.equals("WIFI")) {
            doBindService();

            new Handler().post(mmmQueueCommands);
        }
    }

    private void doBindService() {
        if (!isServiceBound) {
            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(DTCsActivity.this, 4);
            try {
                service.startService();
            } catch (IOException ioe) {
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            isServiceBound = false;
        }
    };

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null && isServiceBound) {
                //obdStatusTextView.setText(cmdResult.toLowerCase());
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.BROKEN_PIPE)) {
            //if (isServiceBound)
            //stopLiveData();
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
            //if(isServiceBound)
            //obdStatusTextView.setText(getString(R.string.status_obd_data));
        }

        /*if (vv.findViewWithTag(cmdID) != null) {
            TextView existingTV = (TextView) vv.findViewWithTag(cmdID);
            existingTV.setText(cmdResult);
        } else*/ addTableRow(cmdID, cmdName, cmdResult);
        //commandResult.put(cmdID, cmdResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isServiceBound) {
            doUnbindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    private void addTableRow(String id, String key, String val) {
        if (id == "Timeout") {
            service.queueJob(new ObdCommandJob(new VinCommand()));
            service.queueJob(new ObdCommandJob(new TroubleCodesCommand()));
        }

        if (id == "VIN") {
            VIN = val;
        }

        if (id == "TROUBLE_CODES") {
            if (!val.equals("")) {
                if (val.charAt(0) == 'P' || val.charAt(0) == 'C' || val.charAt(0) == 'U' || val.charAt(0) == 'B') {
                    String[] splitted = val.split("\n");
                    if (dtcs.size() == splitted.length) {
                        dtcs.clear();
                    }
                    for (int i = 0; i < splitted.length; i++) {
                        //DTC dtc = new DTC(splitted[i], splitted[i]);
                        databaseAccess.open();
                        tblDTC dtc = databaseAccess.getDTC(splitted[i]);
                        databaseAccess.close();
                        if (dtc != null) {
                            //dtcs.put(splitted[i], splitted[i]);
                            dtcs.put(dtc.getCode(), dtc.getDescription());
                        } else {
                            dtcs.put(splitted[i], "Not included in database");
                        }

                    }
                    keepRunning = 0;
                    maxL = dtcs.size();
                    recyclerView.setAdapter(new DTCsAdapter(dtcs, R.layout.list_item_dtc, getApplicationContext()));
                    btnDTC.setEnabled(true);
                }
            } else {
                //count = count + 1;
                //if (count >= 3) {
                keepRunning = 0;
                dtcs.put("No DTCs", "No DTC Codes found");
                recyclerView.setAdapter(new DTCsAdapter(dtcs, R.layout.list_item_dtc, getApplicationContext()));
                //}
            }
        }

        if (id == "CLEAR_TROUBLE_CODES") {
            Toast.makeText(DTCsActivity.this, "DTC codes cleared", Toast.LENGTH_LONG).show();
            btnDTC.setEnabled(false);
        }
    }

    /*public void getTaxonomy() {
        Taxonomy tx = new Taxonomy();
        for (String code : dtcs.keySet()) {
            if (code.indexOf("P") != -1) {
                tx.setSystemID(7);
                //if (dtcs.get(code).indexOf())
            }
        }
    }*/

    /*public void sendDTCs() {
        Call<ResponseBody> userCall = apiService.sendDTCs(VIN, dtcs);

        userCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("onResponse", "" + response.code());


                if(response.code() == 200) {
                    Toast.makeText(DTCsActivity.this, "DTCs uploaded successfully", Toast.LENGTH_LONG).show();
                    /*Intent intent = new Intent(DTCsActivity.this, HomeAutoOwnerActivity.class);
                    intent.putExtra("Token", token);
                    startActivity(intent);
                    //  finish();
                } else {
                    Toast.makeText(DTCsActivity.this, "" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }*/
    /*public void getRepairs() {
        //for (Integer i = 0; i < systemID.length; i++) {
        getRepair(0, maxL);
        //}
    }*/

    /*public void getRepair(Integer index, Integer maxLength) {
        Call<List<Repair>> userCall = apiService.getRepairs(baseVehicleID, engineID, subModelID, systemID[index], groupID[index], subGroupID[index]);

        userCall.enqueue(new Callback<List<Repair>>() {
            @Override
            public void onResponse(Call<List<Repair>> call, Response<List<Repair>> response) {
                Log.d("onResponse", "" + response.code());


                if(response.code() == 200) {
                    if (response.body() != null) {
                        //Toast.makeText(DTCsActivity.this, "Repairs found", Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(DTCsActivity.this, RepairsActivity.class);
                        //intent.putExtra("numOfElements", response.body().size());
                        for (Integer i = 0; i < response.body().size(); i++) {
                            Repair repair = response.body().get(i);
                            repairs.add(repair);
                            //intent.putExtra(i.toString(), repair);
                        }
                        if (index < maxLength - 1) {
                            getRepair(index + 1, maxLength);
                        } else {
                            Intent intent = new Intent(DTCsActivity.this, RepairsActivity.class);
                            intent.putExtra("numOfElements", repairs.size());
                            for (Integer i = 0; i < repairs.size(); i++) {
                                Repair repair = repairs.get(i);
                                intent.putExtra(i.toString(), repair);
                            }
                            startActivity(intent);
                        }
                        //startActivity(intent);
                    }
                    //  finish();
                } else {
                    Toast.makeText(DTCsActivity.this, "" + response.code(), Toast.LENGTH_SHORT).show();
                    getRepair(index, maxLength);
                }
            }

            @Override
            public void onFailure(Call<List<Repair>> call, Throwable t) {
                Log.d("onFailure", t.toString());
                getRepair(index, maxLength);
            }
        });
    }*/

    public void clearDTCs() {
        service.queueJob(new ObdCommandJob(new ClearTroubleCodesCommand()));
        service.queueJob(new ObdCommandJob(new TroubleCodesCommand()));

        dtcs.clear();
        recyclerView.setAdapter(new DTCsAdapter(dtcs, R.layout.list_item_dtc, getApplicationContext()));
        keepRunning = 1;
        count = 0;
        new Handler().postDelayed(mmmQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
    }

    /*public void initializeTaxonomies() {
        systemIDs.add(1);
        systemIDs.add(2);
        systemIDs.add(3);
        systemIDs.add(4);
        systemIDs.add(5);
        systemIDs.add(6);
        systemIDs.add(7);
        systemIDs.add(8);

        systems.add("Body & Frame");
        systems.add("Brakes");
        systems.add("Electrical");
        systems.add("HVAC");
        systems.add("Steering");
        systems.add("Suspension");
        systems.add("Powertrain");
        systems.add("Vehicle");

        groupIDs.add(7);
        groupIDs.add(13);
        groupIDs.add(36);
        groupIDs.add(55);
        groupIDs.add(56);
        groupIDs.add(58);
        groupIDs.add(1);
        groupIDs.add(15);
        groupIDs.add(17);
        groupIDs.add(19);
        groupIDs.add(24);
        groupIDs.add(27);
        groupIDs.add(34);
        groupIDs.add(55);
        groupIDs.add(56);
        groupIDs.add(1);
        groupIDs.add(6);
        groupIDs.add(11);
        groupIDs.add(13);
        groupIDs.add(15);
        groupIDs.add(20);
        groupIDs.add(29);
        groupIDs.add(30);
        groupIDs.add(32);
        groupIDs.add(38);
        groupIDs.add(52);
        groupIDs.add(55);
        groupIDs.add(56);
        groupIDs.add(58);
        groupIDs.add(89);
        groupIDs.add(2);
        groupIDs.add(12);
        groupIDs.add(15);
        groupIDs.add(21);
        groupIDs.add(56);
        groupIDs.add(16);
        groupIDs.add(24);
        groupIDs.add(28);
        groupIDs.add(58);
        groupIDs.add(1);
        groupIDs.add(9);
        groupIDs.add(15);
        groupIDs.add(22);
        groupIDs.add(23);
        groupIDs.add(25);
        groupIDs.add(28);
        groupIDs.add(33);
        groupIDs.add(55);
        groupIDs.add(58);
        groupIDs.add(15);
        groupIDs.add(18);
        groupIDs.add(20);
        groupIDs.add(31);
        groupIDs.add(41);
        groupIDs.add(42);
        groupIDs.add(55);
        groupIDs.add(58);
        groupIDs.add(66);
        groupIDs.add(70);
        groupIDs.add(1);
        groupIDs.add(80);

        groups.add("Passenger Restraint System");
        groups.add("Body Panels");
        groups.add("Interior Trim & Panels");
        groups.add("Sensors");
        groups.add("Switches");
        groups.add("Control Module");
        groups.add("@ALL");
        groups.add("Control System");
        groups.add("Disc Brakes");
        groups.add("Drum Brakes");
        groups.add("Hydraulic System");
        groups.add("Parking Brake");
        groups.add("Power Assist");
        groups.add("Sensors");
        groups.add("Switches");
        groups.add("@ALL");
        groups.add("Exterior Lighting");
        groups.add("Wiper & Washer System");
        groups.add("Body Panels");
        groups.add("Control System");
        groups.add("Engine");
        groups.add("Power Distribution");
        groups.add("Steering Column");
        groups.add("Warning Systems");
        groups.add("Latches & Locks");
        groups.add("Driver Information System");
        groups.add("Sensors");
        groups.add("Switches");
        groups.add("Control Module");
        groups.add("Body Control System");
        groups.add("Air Conditioning System");
        groups.add("Air Distribution");
        groups.add("Control System");
        groups.add("Heating");
        groups.add("Switches");
        groups.add("Directional Control");
        groups.add("Hydraulic System");
        groups.add("Pivot Points");
        groups.add("Control Module");
        groups.add("@ALL");
        groups.add("Tire & Wheel");
        groups.add("Control System");
        groups.add("Vertical Dampening");
        groups.add("Hub & Bearings");
        groups.add("Lateral Dampening");
        groups.add("Pivot Points");
        groups.add("Adjustments");
        groups.add("Sensors");
        groups.add("Control Module");
        groups.add("Control System");
        groups.add("Driveline");
        groups.add("Engine");
        groups.add("Trans");
        groups.add("Automatic Trans");
        groups.add("Manual Trans");
        groups.add("Sensors");
        groups.add("Switches");
        groups.add("Control Module");
        groups.add("Hybrid Powertrain System");
        groups.add("Relays");
        groups.add("@ALL");
        groups.add("Universal Vehicle Operation");
    }*/

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    doBindService();

                    new Handler().post(mmmQueueCommands);
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
}
