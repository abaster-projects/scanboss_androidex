package com.repairboss.scanboss.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.TableLayout;
import android.widget.TextView;

import com.repairboss.scanboss.io.AbstractGatewayService;
import com.repairboss.scanboss.io.ObdCommandJob;
import com.repairboss.scanboss.io.ObdGatewayService;
import com.repairboss.scanboss.io.ObdProgressListener;
import com.scanboss.obd.commands.control.DtcNumberCommand;
import com.scanboss.obd.commands.control.VinCommand;
import com.scanboss.obd.enums.AvailableCommandNames;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;

public class ECUConnectionActivity extends AppCompatActivity implements ObdProgressListener {

    private static final int TABLE_ROW_MARGIN = 7;

    static {
        RoboGuice.setUseAnnotationDatabases(false);
    }

    public Map<String, String> commandResult = new HashMap<String, String>();
    private boolean isServiceBound;
    private TableLayout tl;
    private AbstractGatewayService service;
    private AVLoadingIndicatorView avi;
    private TextView tvVINECU;
    private TextView tvConnected;
    private TextView tvNumDTCs;

    @Inject
    SharedPreferences prefs;

    private final Runnable mmQueueCommands = new Runnable() {
        public void run() {
            //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            //if (connectionType.equals("BLUETOOTH")) {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                service.queueJob(new ObdCommandJob(new VinCommand()));
                service.queueJob(new ObdCommandJob(new DtcNumberCommand()));
            }
            /*} else if (connectionType.equals("WIFI")) {
                if (serviceWiFi != null && serviceWiFi.isRunning() && serviceWiFi.queueEmpty()) {
                    serviceWiFi.queueJob(new ObdCommandJob(new VinCommand()));
                    serviceWiFi.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                }
            }*/
            // run again in period defined in preferences
            //new Handler().postDelayed(mQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecuconnection);

        prefs = getSharedPreferences("com.repairboss.scanboss_preferences", MODE_PRIVATE);

        //tl = findViewById(R.id.data_table_ecu);
        tvVINECU = findViewById(R.id.VINECU);
        tvConnected = findViewById(R.id.Connected);
        tvNumDTCs = findViewById(R.id.card_num_dtcs_connect);
        avi = findViewById(R.id.avi);

        //tl.removeAllViews();

        startAnim();

        doBindService();

        new Handler().post(mmQueueCommands);
    }

    private void doBindService() {
        if (!isServiceBound) {
            //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            //if (connectionType.equals("BLUETOOTH")) {
            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            /*} else if (connectionType.equals("WIFI")) {
                Intent serviceIntent = new Intent(this, ObdGatewayWiFiService.class);
                bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            }*/
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            //if (connectionType.equals("BLUETOOTH")) {
            if (service.isRunning()) {
                service.stopService();
            }
            unbindService(serviceConn);
            /*} else if (connectionType.equals("WIFI")) {
                if (serviceWiFi.isRunning()) {
                    serviceWiFi.stopService();
                }
                unbindService(serviceConn);
            }*/
            isServiceBound = false;
        }
    }

    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            //if (connectionType.equals("BLUETOOTH")) {
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(ECUConnectionActivity.this, 1);
            try {
                service.startService();
            } catch (IOException ioe) {
                doUnbindService();
            }
            /*} else if (connectionType.equals("WIFI")) {
                serviceWiFi = ((AbstractGatewayWiFiService.AbstractGatewayWiFiServiceBinder) binder).getService();
                serviceWiFi.setContext(ECUConnectionActivity.this, 1);
                try {
                    serviceWiFi.startService();
                } catch (IOException ioe) {
                    doUnbindService();
                }
            }*/
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            isServiceBound = false;
        }
    };

    /*private ServiceConnection serviceConnWiFi = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            serviceWiFi = ((AbstractGatewayWiFiService.AbstractGatewayWiFiServiceBinder) binder).getService();
            serviceWiFi.setContext(ECUConnectionActivity.this, 1);
            try {
                serviceWiFi.startService();
            } catch (IOException ioe) {
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            isServiceBound = false;
        }
    };*/

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null && isServiceBound) {
                //obdStatusTextView.setText(cmdResult.toLowerCase());
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.BROKEN_PIPE)) {
            //if (isServiceBound)
            //stopLiveData();
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
            //if(isServiceBound)
            //obdStatusTextView.setText(getString(R.string.status_obd_data));
        }

        /*if (vv.findViewWithTag(cmdID) != null) {
            TextView existingTV = (TextView) vv.findViewWithTag(cmdID);
            existingTV.setText(cmdResult);
        } else*/ addTableRow(cmdID, cmdName, cmdResult);
        //commandResult.put(cmdID, cmdResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isServiceBound) {
            doUnbindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    private void addTableRow(String id, String key, String val) {

        /*TableRow tr = new TableRow(this);
        ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(TABLE_ROW_MARGIN, TABLE_ROW_MARGIN, TABLE_ROW_MARGIN,
                TABLE_ROW_MARGIN);
        tr.setLayoutParams(params);

        TextView name = new TextView(this);
        name.setGravity(Gravity.RIGHT);
        name.setText(key + ": ");
        TextView value = new TextView(this);
        value.setGravity(Gravity.LEFT);
        value.setText(val);
        value.setTag(id);
        tr.addView(name);
        tr.addView(value);
        tl.addView(tr, params);*/

        if (id == "Timeout") {
            //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            //if (connectionType.equals("BLUETOOTH")) {
            service.queueJob(new ObdCommandJob(new VinCommand()));
            service.queueJob(new ObdCommandJob(new DtcNumberCommand()));
            /*} else if (connectionType.equals("WIFI")) {
                serviceWiFi.queueJob(new ObdCommandJob(new VinCommand()));
                serviceWiFi.queueJob(new ObdCommandJob(new DtcNumberCommand()));
            }*/
        }

        if (id == "VIN") {
            if (!val.equals("")) {
                if ((val.charAt(0) == '.') || (val.charAt(0) == 'B')) {
                    //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
                    //if (connectionType.equals("BLUETOOTH")) {
                    service.queueJob(new ObdCommandJob(new VinCommand()));
                    service.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                    /*} else if (connectionType.equals("WIFI")) {
                        serviceWiFi.queueJob(new ObdCommandJob(new VinCommand()));
                        serviceWiFi.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                    }*/
                } else {
                    tvVINECU.setText(val);
                    tvConnected.setText("Connected");
                    stopAnim();
                }
            }
        } else if (id == "DTC_NUMBER") {
            if (!val.equals("")) {
                if (val.charAt(0) != ':') {
                    char temp;
                    if (val.charAt(8) == 'N') {
                        temp = val.charAt(9);
                    } else {
                        temp = val.charAt(10);
                    }
                    tvNumDTCs.setText(String.valueOf(temp));
                } else {
                    service.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                }
            } else {
                //final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
                //if (connectionType.equals("BLUETOOTH")) {
                service.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                /*} else if (connectionType.equals("WIFI")) {
                    serviceWiFi.queueJob(new ObdCommandJob(new DtcNumberCommand()));
                }*/
            }
        }
    }

    public void startAnim() {
        avi.show();
    }

    public void stopAnim() {
        avi.hide();
    }
}
