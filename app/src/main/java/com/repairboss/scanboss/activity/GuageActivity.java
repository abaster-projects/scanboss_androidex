package com.repairboss.scanboss.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.SingleValueDataSet;
import com.anychart.charts.CircularGauge;
import com.anychart.enums.Anchor;
import com.anychart.graphics.vector.text.HAlign;
import com.repairboss.scanboss.io.AbstractGatewayService;
import com.repairboss.scanboss.io.ObdCommandJob;
import com.repairboss.scanboss.io.ObdGatewayService;
import com.scanboss.obd.commands.SpeedCommand;
import com.scanboss.obd.commands.control.VinCommand;
import com.scanboss.obd.enums.AvailableCommandNames;

import java.io.IOException;

import javax.inject.Inject;

public class GuageActivity extends AppCompatActivity {

    private boolean isServiceBound;
    private AbstractGatewayService service;
    @Inject
    SharedPreferences prefs;
    AnyChartView anyChartView;

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                service.queueJob(new ObdCommandJob(new SpeedCommand()));
            }
            // run again in period defined in preferences
            new Handler().postDelayed(mQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guage);
        prefs = getSharedPreferences("com.repairboss.scanboss_preferences", MODE_PRIVATE);

        doBindService();

        new Handler().post(mQueueCommands);

        anyChartView = findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(findViewById(R.id.progress_bar));


    }

    private void doBindService() {
        if (!isServiceBound) {
            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }

    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(GuageActivity.this, 3);
            try {
                service.startService();
            } catch (IOException ioe) {
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            isServiceBound = false;
        }
    };

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null && isServiceBound) {
                //obdStatusTextView.setText(cmdResult.toLowerCase());
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.BROKEN_PIPE)) {
            //if (isServiceBound)
            //stopLiveData();
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
            //if(isServiceBound)
            //obdStatusTextView.setText(getString(R.string.status_obd_data));
        }

        /*if (vv.findViewWithTag(cmdID) != null) {
            TextView existingTV = (TextView) vv.findViewWithTag(cmdID);
            existingTV.setText(cmdResult);
        } else*/ addTableRow(cmdID, cmdName, cmdResult);
        //commandResult.put(cmdID, cmdResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isServiceBound) {
            doUnbindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    private void addTableRow(String id, String key, String val) {

        /*TableRow tr = new TableRow(this);
        ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(TABLE_ROW_MARGIN, TABLE_ROW_MARGIN, TABLE_ROW_MARGIN,
                TABLE_ROW_MARGIN);
        tr.setLayoutParams(params);

        TextView name = new TextView(this);
        name.setGravity(Gravity.RIGHT);
        name.setText(key + ": ");
        TextView value = new TextView(this);
        value.setGravity(Gravity.LEFT);
        value.setText(val);
        value.setTag(id);
        tr.addView(name);
        tr.addView(value);
        tl.addView(tr, params);*/

        if (id == "SPEED") {
            //double currentValue = val;
            CircularGauge circularGauge = AnyChart.circular();
            circularGauge.fill("#fff")
                    .stroke(null)
                    .padding(0, 0, 0, 0)
                    .margin(30, 30, 30, 30);
            circularGauge.startAngle(0)
                    .sweepAngle(360);

            double currentValue = 13.8D;
            circularGauge.data(new SingleValueDataSet(new Double[] { currentValue }));
            //circularGauge.data(new SingleValueDataSet(new Double[] {Double.valueOf(val)}));

            circularGauge.axis(0)
                    .startAngle(-150)
                    .radius(80)
                    .sweepAngle(300)
                    .width(3)
                    .ticks("{ type: 'line', length: 4, position: 'outside' }");

            circularGauge.axis(0).labels().position("outside");

            circularGauge.axis(0).scale()
                    .minimum(0)
                    .maximum(140);

            circularGauge.axis(0).scale()
                    .ticks("{interval: 10}")
                    .minorTicks("{interval: 10}");

            circularGauge.needle(0)
                    .stroke(null)
                    .startRadius("6%")
                    .endRadius("38%")
                    .startWidth("2%")
                    .endWidth(0);

            circularGauge.cap()
                    .radius("4%")
                    .enabled(true)
                    .stroke(null);

            circularGauge.label(0)
                    .text("<span style=\"font-size: 25\">Vehicle Speed</span>")
                    .useHtml(true)
                    .hAlign(HAlign.CENTER);
            circularGauge.label(0)
                    .anchor(Anchor.CENTER_TOP)
                    .offsetY(100)
                    .padding(15, 20, 0, 0);

            circularGauge.label(1)
                    .text("<span style=\"font-size: 20\">" + val + "</span>")
                    .useHtml(true)
                    .hAlign(HAlign.CENTER);
            circularGauge.label(1)
                    .anchor(Anchor.CENTER_TOP)
                    .offsetY(-100)
                    .padding(5, 10, 0, 0)
                    .background("{fill: 'none', stroke: '#c1c1c1', corners: 3, cornerType: 'ROUND'}");

            circularGauge.range(0,
                    "{\n" +
                            "    from: 0,\n" +
                            "    to: 25,\n" +
                            "    position: 'inside',\n" +
                            "    fill: 'green 0.5',\n" +
                            "    stroke: '1 #000',\n" +
                            "    startSize: 6,\n" +
                            "    endSize: 6,\n" +
                            "    radius: 80,\n" +
                            "    zIndex: 1\n" +
                            "  }");

            circularGauge.range(1,
                    "{\n" +
                            "    from: 80,\n" +
                            "    to: 140,\n" +
                            "    position: 'inside',\n" +
                            "    fill: 'red 0.5',\n" +
                            "    stroke: '1 #000',\n" +
                            "    startSize: 6,\n" +
                            "    endSize: 6,\n" +
                            "    radius: 80,\n" +
                            "    zIndex: 1\n" +
                            "  }");

            anyChartView.setChart(circularGauge);
        }
    }
}
