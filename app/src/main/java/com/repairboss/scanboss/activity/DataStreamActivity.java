package com.repairboss.scanboss.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.repairboss.scanboss.config.ObdConfig;
import com.repairboss.scanboss.io.AbstractGatewayService;
import com.repairboss.scanboss.io.ObdCommandJob;
import com.repairboss.scanboss.io.ObdGatewayService;
import com.scanboss.obd.commands.ObdCommand;
import com.scanboss.obd.commands.control.VinCommand;
import com.scanboss.obd.enums.AvailableCommandNames;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class DataStreamActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 3;
    private static final int TABLE_ROW_MARGIN = 7;

    private BluetoothAdapter mBluetoothAdapter = null;

    //public Map<String, String> commandResult = new HashMap<String, String>();
    private boolean isServiceBound;
    //private TableLayout tl;
    private AbstractGatewayService service;
    @Inject
    SharedPreferences prefs;
    private TextView tvVIN;
    private TextView tvVehicleSpeed;
    private TextView tvEngineRPM;
    private TextView tvEngineLoad;
    private TextView tvEngineRunTime;
    private TextView tvThrottlePosition;
    private TextView tvFuelType;
    private TextView tvFuelConsumptionRate;
    private TextView tvFuelLevel;
    private TextView tvMassAirFlow;
    private TextView tvAirFuelRatio;
    private TextView tvWBAirFuelRatio;
    private TextView tvEngineOilTemp;
    private TextView tvBaroPressure;
    private TextView tvFuelPressure;
    private TextView tvFuelRailPressure;
    private TextView tvIntakeManifoldPressure;
    private TextView tvAirIntakeTemp;
    private TextView tvAmbientAirTemp;
    private TextView tvEngineCoolentTemp;
    private TextView tvCMPowerSupply;
    private TextView tvCEquRatio;
    private TextView tvDistTravelinMILOn;
    private TextView tvTimingAdvance;
    //private TextView tvTroubleCodes;
    //private TextView tvDiagTroubleCodes;


    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                queueCommands();
            }
            // run again in period defined in preferences
            new Handler().postDelayed(mQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_stream);
        prefs = getSharedPreferences("com.repairboss.scanboss_preferences", MODE_PRIVATE);

        //tl = findViewById(R.id.data_table_data_stream);

        //tl.removeAllViews();

        tvVIN = findViewById(R.id.VIN);
        tvVehicleSpeed = findViewById(R.id.SPEED);
        tvEngineRPM = findViewById(R.id.ENGINE_RPM);
        tvEngineLoad = findViewById(R.id.ENGINE_LOAD);
        tvEngineRunTime = findViewById(R.id.ENGINE_RUNTIME);
        tvThrottlePosition = findViewById(R.id.THROTTLE_POS);
        tvFuelType = findViewById(R.id.FUEL_TYPE);
        tvFuelConsumptionRate = findViewById(R.id.FUEL_CONSUMPTION_RATE);
        tvFuelLevel = findViewById(R.id.FUEL_LEVEL);
        tvMassAirFlow = findViewById(R.id.MAF);
        tvAirFuelRatio = findViewById(R.id.AIR_FUEL_RATIO);
        tvWBAirFuelRatio = findViewById(R.id.WIDEBAND_AIR_FUEL_RATIO);
        tvEngineOilTemp = findViewById(R.id.ENGINE_OIL_TEMP);
        tvBaroPressure = findViewById(R.id.BAROMETRIC_PRESSURE);
        tvFuelPressure = findViewById(R.id.FUEL_PRESSURE);
        tvFuelRailPressure = findViewById(R.id.FUEL_RAIL_PRESSURE);
        tvIntakeManifoldPressure = findViewById(R.id.INTAKE_MANIFOLD_PRESSURE);
        tvAirIntakeTemp = findViewById(R.id.AIR_INTAKE_TEMP);
        tvAmbientAirTemp = findViewById(R.id.AMBIENT_AIR_TEMP);
        tvEngineCoolentTemp = findViewById(R.id.ENGINE_COOLANT_TEMP);
        tvCMPowerSupply = findViewById(R.id.CONTROL_MODULE_VOLTAGE);
        tvCEquRatio = findViewById(R.id.EQUIV_RATIO);
        tvDistTravelinMILOn = findViewById(R.id.DISTANCE_TRAVELED_MIL_ON);
        tvTimingAdvance = findViewById(R.id.TIMING_ADVANCE);
        //tvTroubleCodes = findViewById(R.id.TROUBLE_CODES);
        //tvDiagTroubleCodes = findViewById(R.id.DTC_NUMBER);

        final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
        if (connectionType.equals("BLUETOOTH")) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
                finish();
            }

            if (mBluetoothAdapter == null) {
                return;
            }
            // If BT is not on, request that it be enabled.
            // setupChat() will then be called during onActivityResult
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } else {
                doBindService();

                new Handler().post(mQueueCommands);
            }
        } else if (connectionType.equals("WIFI")) {

            doBindService();

            new Handler().post(mQueueCommands);
        }
    }

    private void queueCommands() {
        if (isServiceBound) {
            for (ObdCommand Command : ObdConfig.getCommands()) {
                if (prefs.getBoolean(Command.getName(), true))
                    service.queueJob(new ObdCommandJob(Command));
            }
        }
    }

    private void doBindService() {
        if (!isServiceBound) {
            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }

    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(DataStreamActivity.this, 2);
            try {
                service.startService();
            } catch (IOException ioe) {
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            isServiceBound = false;
        }
    };

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null && isServiceBound) {
                //obdStatusTextView.setText(cmdResult.toLowerCase());
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.BROKEN_PIPE)) {
            //if (isServiceBound)
            //stopLiveData();
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
            //if(isServiceBound)
            //obdStatusTextView.setText(getString(R.string.status_obd_data));
        }

        /*if (vv.findViewWithTag(cmdID) != null) {
            TextView existingTV = (TextView) vv.findViewWithTag(cmdID);
            existingTV.setText(cmdResult);
        } else*/ addTableRow(cmdID, cmdName, cmdResult);
        //commandResult.put(cmdID, cmdResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isServiceBound) {
            doUnbindService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    private void addTableRow(String id, String key, String val) {

        /*TableRow tr = new TableRow(this);
        ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(TABLE_ROW_MARGIN, TABLE_ROW_MARGIN, TABLE_ROW_MARGIN,
                TABLE_ROW_MARGIN);
        tr.setLayoutParams(params);

        TextView name = new TextView(this);
        name.setGravity(Gravity.RIGHT);
        name.setText(key + ": ");
        TextView value = new TextView(this);
        value.setGravity(Gravity.LEFT);
        value.setText(val);
        value.setTag(id);
        tr.addView(name);
        tr.addView(value);
        tl.addView(tr, params);*/

        if (id == "VIN") {
            if (!val.equals("")) {
                if ((val.charAt(0) != '.') && (val.charAt(0) != 'B')) {
                    //if (val.charAt(0) != '.') {
                    tvVIN.setText(val);
                }
            }
        } else if (id == "SPEED") {
            if (val.charAt(0) != ':') {
                tvVehicleSpeed.setText(val);
            }
        } else if (id == "ENGINE_RPM") {
            tvEngineRPM.setText(val);
        } else if (id == "ENGINE_LOAD") {
            tvEngineLoad.setText(val);
        } else if (id == "ENGINE_RUNTIME") {
            tvEngineRunTime.setText(val);
        } else if (id == "THROTTLE_POS") {
            tvThrottlePosition.setText(val);
        } else if (id == "FUEL_TYPE") {
            tvFuelType.setText(val);
        } else if (id == "FUEL_CONSUMPTION_RATE") {
            tvFuelConsumptionRate.setText(val);
        } else if (id == "FUEL_LEVEL") {
            tvFuelLevel.setText(val);
        } else if (id == "MAF") {
            tvMassAirFlow.setText(val);
        } else if (id == "AIR_FUEL_RATIO") {
            tvAirFuelRatio.setText(val);
        } else if (id == "WIDEBAND_AIR_FUEL_RATIO") {
            tvWBAirFuelRatio.setText(val);
        } else if (id == "ENGINE_OIL_TEMP") {
            tvEngineOilTemp.setText(val);
        } else if (id == "BAROMETRIC_PRESSURE") {
            tvBaroPressure.setText(val);
        } else if (id == "FUEL_PRESSURE") {
            tvFuelPressure.setText(val);
        } else if (id == "FUEL_RAIL_PRESSURE") {
            tvFuelRailPressure.setText(val);
        } else if (id == "INTAKE_MANIFOLD_PRESSURE") {
            tvIntakeManifoldPressure.setText(val);
        } else if (id == "AIR_INTAKE_TEMP") {
            tvAirIntakeTemp.setText(val);
        } else if (id == "AMBIENT_AIR_TEMP") {
            tvAmbientAirTemp.setText(val);
        } else if (id == "ENGINE_COOLANT_TEMP") {
            tvEngineCoolentTemp.setText(val);
        } else if (id == "CONTROL_MODULE_VOLTAGE") {
            tvCMPowerSupply.setText(val);
        } else if (id == "EQUIV_RATIO") {
            tvCEquRatio.setText(val);
        } else if (id == "DISTANCE_TRAVELED_MIL_ON") {
            tvDistTravelinMILOn.setText(val);
        } else if (id == "TIMING_ADVANCE") {
            tvTimingAdvance.setText(val);
        } else if (id == "TROUBLE_CODES") {
            //tvTroubleCodes.setText(val);
        } else if (id == "DTC_NUMBER") {
            //tvDiagTroubleCodes.setText(val);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    doBindService();

                    new Handler().post(mQueueCommands);
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
}
