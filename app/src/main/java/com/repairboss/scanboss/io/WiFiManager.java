package com.repairboss.scanboss.io;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;

public class WiFiManager {
    public static Socket connect(String serverIP, int serverPort) throws IOException {
        Socket sock = null;
        Socket sockFallback = null;

        InetAddress serverAddress = InetAddress.getByName(serverIP);

        try {
            sock = new Socket(serverAddress, serverPort);
        } catch (Exception e1) {
            try {
                sockFallback = new Socket(serverAddress, serverPort);
                sock = sockFallback;
            } catch (Exception e2) {
                throw new IOException(e2.getMessage());
            }
        }

        return sock;
    }
}
