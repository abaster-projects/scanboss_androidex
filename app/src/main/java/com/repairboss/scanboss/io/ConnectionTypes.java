package com.repairboss.scanboss.io;

public enum ConnectionTypes {
    BLUETOOTH('0'),
    WIFI('1');

    private final char value;

    ConnectionTypes(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
}
