package com.repairboss.scanboss.io;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.repairboss.scanboss.activity.R;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DTCsAdapter extends RecyclerView.Adapter<DTCsAdapter.DTCsViewHolder> {
    private Map<String, String> dtcs;
    private int rowLayout;
    private Context context;

    public static class DTCsViewHolder extends RecyclerView.ViewHolder {
        LinearLayout dtcsLayout;
        TextView dtcCode;
        TextView dtcDescription;


        public DTCsViewHolder(View view) {
            super(view);
            dtcsLayout = view.findViewById(R.id.dtcListLayout);
            dtcCode = view.findViewById(R.id.tvListItemDTCCode);
            dtcDescription = view.findViewById(R.id.tvListItemDTCDescription);
        }

    }

    public DTCsAdapter(Map<String, String> dtcs, int rowLayout, Context context) {
        this.dtcs = dtcs;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public DTCsAdapter.DTCsViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new DTCsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(DTCsViewHolder holder, final int position) {
        Set set = dtcs.entrySet();
        Iterator iterator = set.iterator();
        /*while(iterator.hasNext()) {
            Map.Entry mEntry = (Map.Entry)iterator.next();
            holder.dtcCode.setText(" " + mEntry.getKey());
            holder.dtcDescription.setText(mEntry.getValue().toString());
            System.out.print("key is: "+ mEntry.getKey() + " & Value is: ");
            System.out.println(mEntry.getValue());
        }*/

        int i = 0;
        for (String code : dtcs.keySet()) {
            holder.dtcCode.setText(" " + code);
            String desc = dtcs.get(code);
            holder.dtcDescription.setText(desc);
            if (i == position) {
                break;
            }
            i = i + 1;
        }
    }

    @Override
    public int getItemCount() {
        return dtcs.size();
    }
}
