package com.repairboss.scanboss.io;

public interface ObdProgressListener {
    void stateUpdate(final ObdCommandJob job);
}
