package com.repairboss.scanboss.io;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.inject.Inject;
import com.repairboss.scanboss.activity.ConfigActivity;
import com.repairboss.scanboss.activity.DTCsActivity;
import com.repairboss.scanboss.activity.DataStreamActivity;
import com.repairboss.scanboss.activity.ECUConnectionActivity;
import com.repairboss.scanboss.activity.GuageActivity;
import com.repairboss.scanboss.activity.MainActivity;
import com.repairboss.scanboss.activity.R;
import com.scanboss.obd.commands.protocol.EchoOffCommand;
import com.scanboss.obd.commands.protocol.LineFeedOffCommand;
import com.scanboss.obd.commands.protocol.ObdResetCommand;
import com.scanboss.obd.commands.protocol.SelectProtocolCommand;
import com.scanboss.obd.commands.protocol.TimeoutCommand;
import com.scanboss.obd.commands.temperature.AmbientAirTemperatureCommand;
import com.scanboss.obd.enums.ObdProtocols;
import com.repairboss.scanboss.io.ObdCommandJob.ObdCommandJobState;
import com.scanboss.obd.exceptions.UnsupportedCommandException;

import java.io.IOException;
import java.net.Socket;

public class ObdGatewayService extends AbstractGatewayService {
    @Inject
    SharedPreferences prefs;

    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;

    private Socket socket = null;

    public void startService() throws IOException {
        final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
        if (connectionType.equals("BLUETOOTH")) {
            final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
            if (remoteDevice == null || "".equals(remoteDevice)) {
                Toast.makeText(ctx, getString(R.string.text_bluetooth_nodevice), Toast.LENGTH_LONG).show();

                // TODO kill this service gracefully
                stopService();
                throw new IOException();
            } else {

                final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                dev = btAdapter.getRemoteDevice(remoteDevice);

                btAdapter.cancelDiscovery();

                try {
                    startObdConnection();
                } catch (Exception e) {
                    stopService();
                    throw new IOException();
                }
            }
        } else if (connectionType.equals("WIFI")) {
            try {
                startObdConnection();
            } catch (Exception e) {
                stopService();
                throw new IOException();
            }
        }
    }

    private void startObdConnection() throws IOException {
        isRunning = true;
        try {
            final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
            if (connectionType.equals("BLUETOOTH")) {
                sock = BluetoothManager.connect(dev);
            } else if (connectionType.equals("WIFI")) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            socket = WiFiManager.connect("192.168.4.1", 23);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        } catch (Exception e2) {
            Toast.makeText(ctx, "Device not selected correctly. Please check in settings", Toast.LENGTH_LONG).show();
            stopService();
            throw new IOException();
        }

        // Let's configure the connection.
        queueJob(new ObdCommandJob(new ObdResetCommand()));

        //Below is to give the adapter enough time to reset before sending the commands, otherwise the first startup commands could be ignored.
        try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }

        queueJob(new ObdCommandJob(new EchoOffCommand()));

        /*
         * Will send second-time based on tests.
         *
         * TODO this can be done w/o having to queue jobs by just issuing
         * command.run(), command.getResult() and validate the result.
         */
        queueJob(new ObdCommandJob(new EchoOffCommand()));
        queueJob(new ObdCommandJob(new LineFeedOffCommand()));
        queueJob(new ObdCommandJob(new TimeoutCommand(62)));

        // Get protocol from preferences
        final String protocol = prefs.getString(ConfigActivity.PROTOCOLS_LIST_KEY, "AUTO");
        queueJob(new ObdCommandJob(new SelectProtocolCommand(ObdProtocols.valueOf(protocol))));

        queueJob(new ObdCommandJob(new EchoOffCommand()));
        // Job for returning dummy data
        //queueJob(new ObdCommandJob(new AmbientAirTemperatureCommand()));

        queueCounter = 0L;
    }

    @Override
    public void queueJob(ObdCommandJob job) {
        // This is a good place to enforce the imperial units option
        job.getCommand().useImperialUnits(prefs.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY, false));

        // Now we can pass it along
        super.queueJob(job);
    }

    protected void executeQueue() throws InterruptedException {
        while (!Thread.currentThread().isInterrupted()) {
            ObdCommandJob job = null;
            try {
                job = jobsQueue.take();
                if (job.getState().equals(ObdCommandJobState.NEW)) {
                    job.setState(ObdCommandJobState.RUNNING);
                    final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
                    if (connectionType.equals("BLUETOOTH")) {
                        if (sock.isConnected()) {
                            job.getCommand().run(sock.getInputStream(), sock.getOutputStream());
                        } else {
                            job.setState(ObdCommandJobState.EXECUTION_ERROR);
                        }
                    } else if (connectionType.equals("WIFI")) {
                        if (socket.isConnected()) {
                            job.getCommand().run(socket.getInputStream(), socket.getOutputStream());
                        } else {
                            job.setState(ObdCommandJobState.EXECUTION_ERROR);
                        }
                    }
                }
            } catch (InterruptedException i) {
                Thread.currentThread().interrupt();
            } catch (UnsupportedCommandException u) {
                if (job != null) {
                    job.setState(ObdCommandJobState.NOT_SUPPORTED);
                }
            } catch (IOException io) {
                if (job != null) {
                    if(io.getMessage().contains("Broken pipe"))
                        job.setState(ObdCommandJobState.BROKEN_PIPE);
                    else
                        job.setState(ObdCommandJobState.EXECUTION_ERROR);
                }
            } catch (Exception e) {
                if (job != null) {
                    job.setState(ObdCommandJobState.EXECUTION_ERROR);
                }
            }

            if (job != null) {
                final ObdCommandJob job2 = job;
                if (cInt == 1) {
                    ((ECUConnectionActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //((ECUConnectionActivity) ctx).stopAnim();
                            ((ECUConnectionActivity) ctx).stateUpdate(job2);

                        }
                    });
                } else if (cInt == 2) {
                    ((DataStreamActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((DataStreamActivity) ctx).stateUpdate(job2);
                        }
                    });
                } else if (cInt == 3) {
                    ((GuageActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((GuageActivity) ctx).stateUpdate(job2);
                        }
                    });
                } else if (cInt == 4) {
                    ((DTCsActivity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((DTCsActivity) ctx).stateUpdate(job2);
                        }
                    });
                }
            }
        }
    }

    public void stopService() {
        jobsQueue.clear();
        isRunning = false;

        final String connectionType = prefs.getString(ConfigActivity.CONNECTION_TYPE_KEY, "BLUETOOTH");
        if (connectionType.equals("BLUETOOTH")) {
            if (sock != null)
                // close socket
                try {
                    sock.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        } else if (connectionType.equals("WIFI")) {
            if (socket != null)
                // close socket
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        // kill service
        stopSelf();
        startService(new Intent(ctx, MainActivity.class));
        if (cInt == 1) {
            ((ECUConnectionActivity) ctx).finish();
        } else if (cInt == 2) {
            ((DataStreamActivity) ctx).finish();
        }
    }

    public boolean isRunning() {
        return isRunning;
    }
}
