package com.repairboss.scanboss.config;

import com.scanboss.obd.commands.ObdCommand;
import com.scanboss.obd.commands.SpeedCommand;
import com.scanboss.obd.commands.control.DistanceMILOnCommand;
import com.scanboss.obd.commands.control.DtcNumberCommand;
import com.scanboss.obd.commands.control.EquivalentRatioCommand;
import com.scanboss.obd.commands.control.ModuleVoltageCommand;
import com.scanboss.obd.commands.control.TimingAdvanceCommand;
import com.scanboss.obd.commands.control.TroubleCodesCommand;
import com.scanboss.obd.commands.control.VinCommand;
import com.scanboss.obd.commands.engine.LoadCommand;
import com.scanboss.obd.commands.engine.MassAirFlowCommand;
import com.scanboss.obd.commands.engine.OilTempCommand;
import com.scanboss.obd.commands.engine.RPMCommand;
import com.scanboss.obd.commands.engine.RuntimeCommand;
import com.scanboss.obd.commands.engine.ThrottlePositionCommand;
import com.scanboss.obd.commands.fuel.AirFuelRatioCommand;
import com.scanboss.obd.commands.fuel.ConsumptionRateCommand;
import com.scanboss.obd.commands.fuel.FindFuelTypeCommand;
import com.scanboss.obd.commands.fuel.FuelLevelCommand;
import com.scanboss.obd.commands.fuel.FuelTrimCommand;
import com.scanboss.obd.commands.fuel.WidebandAirFuelRatioCommand;
import com.scanboss.obd.commands.pressure.BarometricPressureCommand;
import com.scanboss.obd.commands.pressure.FuelPressureCommand;
import com.scanboss.obd.commands.pressure.FuelRailPressureCommand;
import com.scanboss.obd.commands.pressure.IntakeManifoldPressureCommand;
import com.scanboss.obd.commands.temperature.AirIntakeTemperatureCommand;
import com.scanboss.obd.commands.temperature.AmbientAirTemperatureCommand;
import com.scanboss.obd.commands.temperature.EngineCoolantTemperatureCommand;
import com.scanboss.obd.enums.FuelTrim;

import java.util.ArrayList;

public final class ObdConfig {
    public static ArrayList<ObdCommand> getCommands() {
        ArrayList<ObdCommand> cmds = new ArrayList<>();

        // Control
        cmds.add(new VinCommand());

        // Misc
        cmds.add(new SpeedCommand());

        // Engine
        cmds.add(new RPMCommand());
        cmds.add(new LoadCommand());
        cmds.add(new RuntimeCommand());
        cmds.add(new ThrottlePositionCommand());

        // Fuel
        cmds.add(new FindFuelTypeCommand());
        cmds.add(new ConsumptionRateCommand());
        cmds.add(new FuelLevelCommand());

        // Engine
        cmds.add(new MassAirFlowCommand());

        // Fuel
        cmds.add(new AirFuelRatioCommand());
        cmds.add(new WidebandAirFuelRatioCommand());
        cmds.add(new OilTempCommand());

        // Pressure
        cmds.add(new BarometricPressureCommand());
        cmds.add(new FuelPressureCommand());
        cmds.add(new FuelRailPressureCommand());
        cmds.add(new IntakeManifoldPressureCommand());

        // Temperature
        cmds.add(new AirIntakeTemperatureCommand());
        cmds.add(new AmbientAirTemperatureCommand());
        cmds.add(new EngineCoolantTemperatureCommand());

        // Control
        cmds.add(new ModuleVoltageCommand());
        cmds.add(new EquivalentRatioCommand());
        cmds.add(new DistanceMILOnCommand());
        cmds.add(new DtcNumberCommand());
        cmds.add(new TimingAdvanceCommand());
        cmds.add(new TroubleCodesCommand());


        // cmds.add(new AverageFuelEconomyObdCommand());
        //cmds.add(new FuelEconomyCommand());

        // cmds.add(new FuelEconomyMAPObdCommand());
        // cmds.add(new FuelEconomyCommandedMAPObdCommand());
        //cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_1));
        //cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_2));
        //cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_1));
        //cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_2));


        return cmds;
    }
}
